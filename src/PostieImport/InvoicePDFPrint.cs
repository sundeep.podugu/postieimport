﻿namespace PostieImport
{
    public class InvoicePDFPrint
    {
        public string InvoiceDescription { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string RunNumber { get; set; }
        public string DeliveryName { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliverySuburb { get; set; }
        public string DeliveryState { get; set; }
        public string DeliveryPostcode { get; set; }
        public string ConsultantName { get; set; }
        public string ConsultantPostcode { get; set; }
        public string ConsultantPhone { get; set; }
        public decimal GarmentTotal { get; set; }
    }
}
