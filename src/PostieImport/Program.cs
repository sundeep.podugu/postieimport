﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Configuration;

namespace PostieImport
{
    class Program
    {
        private static string _connectionString = ConfigurationManager.AppSettings["postieinvoicedb"].ToString();
        private static string _sourceFolder = ConfigurationManager.AppSettings["sourcefolder"].ToString();
        private static string _archiveFolder = ConfigurationManager.AppSettings["archivefolder"].ToString();

        static void Main(string[] args)
        {
            var files = Directory.GetFiles(_sourceFolder, "*.txt");
            foreach (var filePath in files)
            {
                try
                {
                    Console.WriteLine("Processing file:" + filePath);
                    var sourceText = File.ReadAllText(filePath);

                    // extract the text which contains print values
                    sourceText = ExtractTextToBeParsedFromSource(sourceText);

                    // parse print and print lines
                    int matchingEndIndex;
                    var print = ParseHeader(sourceText, out matchingEndIndex);
                    print.GarmentTotal = ParseGarmentTotal(sourceText);

                    var printLines = ParsePrintLines(sourceText, matchingEndIndex, print.InvoiceNumber);

                    // import into database
                    ImportPrintIntoDatabase(print);
                    ImportPrintLinesIntoDatabase(printLines);

                    // archive file
                    File.Move(filePath, Path.Combine(_archiveFolder, new FileInfo(filePath).Name));
                    Console.WriteLine("Processing file successfully");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error occured, skipping file: " + filePath);
                    Console.WriteLine("Exception: " + ex.Message);
                }
            }
            Console.WriteLine("Job completed!!!");
            Console.ReadLine();
        }

        private static string ExtractTextToBeParsedFromSource(string sourceText)
        {
            var result = Regex.Match(sourceText, @".+?Total:\|.+?(\n|\r|\r\n)", RegexOptions.Singleline).Value;
            if (string.IsNullOrWhiteSpace(result))
            {
                throw new Exception("Invalid File. Total is not present.");
            }
            return result;
        }

        private static InvoicePDFPrint ParseHeader(string sourceText, out int matchingEndIndex)
        {
            InvoicePDFPrint print = new InvoicePDFPrint();

            string txtHeaderRegex = @"^.*?\|(?<InvoiceDescription>.*?)\|(?<InvoiceNumber>.*?)\|.*?\|(?<RunNumber>.*?)\|(?<DeliveryName>.*?)\|(?<DeliveryAddress>.*?)\|(?<DeliverySuburb>.*?)\|(?<DeliveryState>.*?)\|(?<DeliveryPostcode>.*?)\|(?<ConsultantName>.*?)\|(?<ConsultantPostcode>.*?)\|(?<ConsultantPhone>.*)\|(?<InvoiceDate>.*?)\|.*$";

            Regex headerRegex = new Regex(txtHeaderRegex, RegexOptions.Multiline);
            Match match = headerRegex.Match(sourceText);
            if (match.Success)
            {
                print.InvoiceDescription = match.Groups["InvoiceDescription"].Value;
                print.InvoiceNumber = match.Groups["InvoiceNumber"].Value;
                print.RunNumber = match.Groups["RunNumber"].Value;
                print.DeliveryName = match.Groups["DeliveryName"].Value;
                print.DeliveryAddress = match.Groups["DeliveryAddress"].Value;
                print.DeliverySuburb = match.Groups["DeliverySuburb"].Value;
                print.DeliveryState = match.Groups["DeliveryState"].Value;
                print.DeliveryPostcode = match.Groups["DeliveryPostcode"].Value;
                print.ConsultantName = match.Groups["ConsultantName"].Value;
                print.ConsultantPostcode = match.Groups["ConsultantPostcode"].Value;
                print.ConsultantPhone = match.Groups["ConsultantPhone"].Value;
                print.InvoiceDate = match.Groups["InvoiceDate"].Value;
                matchingEndIndex = match.Index + match.Length;
            }
            else
            {
                throw new Exception("Invalid File. No header present.");
            }

            return print;
        }

        private static List<InvoicePDFPrintLine> ParsePrintLines(string sourceText, int startIndex, string invoiceNumber)
        {
            List<InvoicePDFPrintLine> printLines = new List<InvoicePDFPrintLine>();

            // remove the total
            sourceText = Regex.Replace(sourceText, @"Total:\|.+?(\n|\r|\r\n)", string.Empty);

            // extract guest names
            string txtGuestNameRegex = @"\*\s([\w\s]+)\|$";
            Regex guestNameRegex = new Regex(txtGuestNameRegex, RegexOptions.Multiline);
            var guestNameMatches = guestNameRegex.Matches(sourceText, startIndex);

            // loop through the guest name matches and parse the garments for each guest
            for (int i = 0; i < guestNameMatches.Count; i++)
            {
                string itemsText = string.Empty;
                int startIndexOfLineItem = guestNameMatches[i].Index + guestNameMatches[i].Length;

                // extract text of the list of garments for each guest
                if (i == guestNameMatches.Count - 1)
                {
                    itemsText = sourceText.Substring(startIndexOfLineItem, sourceText.Length - startIndexOfLineItem);
                }
                else
                {
                    itemsText = sourceText.Substring(startIndexOfLineItem, guestNameMatches[i + 1].Index - startIndexOfLineItem);
                }

                // parse each garment
                string txtGarmentRegex = @"(?<GarmentDescription>.*?)\|(?<GarmentSize>.*?)\|(?<GarmentColour>.*?)\|(?<GarmentQtySupplied>.*?)\|(?<GarmentQtyBO>.*?)\|(?<GarmentPriceIncGST>.*?)\|($|(?<GarmentDepositPaid>.*?)\|$)";
                string txtGarmentCommentRegex = @"(?<GarmentComment>.+)$";

                // line can be either with garment values or only garment comment
                Regex regexLineItem = new Regex(txtGarmentRegex + "|" + txtGarmentCommentRegex, RegexOptions.Multiline);

                var garmentMatches = regexLineItem.Matches(itemsText);
                InvoicePDFPrintLine printLine = null;

                for (int j = 0; j < garmentMatches.Count; j++)
                {
                    if (Regex.IsMatch(garmentMatches[j].Value, txtGarmentRegex))
                    {
                        printLine = new InvoicePDFPrintLine();
                        printLine.GuestName = guestNameMatches[i].Groups[1].Value;
                        printLine.InvoiceNumber = invoiceNumber;
                        printLine.GarmentDescription = garmentMatches[j].Groups["GarmentDescription"].Value;
                        printLine.GarmentSize = garmentMatches[j].Groups["GarmentSize"].Value;
                        printLine.GarmentColour = garmentMatches[j].Groups["GarmentColour"].Value;
                        printLine.GarmentQtySupplied = garmentMatches[j].Groups["GarmentQtySupplied"].Value;
                        printLine.GarmentQtyBO = garmentMatches[j].Groups["GarmentQtyBO"].Value;
                        if (decimal.TryParse(garmentMatches[j].Groups["GarmentPriceIncGST"].Value, out decimal priceIncGST))
                        {
                            printLine.GarmentPriceIncGST = priceIncGST;
                        }
                        if (decimal.TryParse(garmentMatches[j].Groups["GarmentDepositPaid"].Value, out decimal depositPaid))
                        {
                            printLine.GarmentDepositPaid = depositPaid;
                        }
                        printLines.Add(printLine);
                    }
                    else
                    {
                        var existingPrintLine = printLines.Where(p => p.InvoiceNumber == printLine.InvoiceNumber && p.GuestName == printLine.GuestName && p.GarmentDescription == printLine.GarmentDescription).SingleOrDefault();
                        existingPrintLine.GarmentComment = garmentMatches[j].Groups["GarmentComment"].Value;
                    }
                }
            }
            return printLines;
        }

        private static decimal ParseGarmentTotal(string sourceText)
        {
            var result = Regex.Match(sourceText, @"Total:\|(?<GarmentTotal>.+?)(\n|\r|\r\n)", RegexOptions.Singleline).Groups["GarmentTotal"].Value;
            if (decimal.TryParse(result, out decimal total))
            {
                return total;
            }

            return 0;
        }

        private static void ImportPrintIntoDatabase(InvoicePDFPrint print)
        {
            string query = @"INSERT INTO [dbo].[InvoicePDFPrint]
                                    ([InvoiceDescription]
                                    ,[InvoiceNumber]
                                    ,[InvoiceDate]
                                    ,[RunNumber]
                                    ,[DeliveryName]
                                    ,[DeliveryAddress]
                                    ,[DeliverySuburb]
                                    ,[DeliveryState]
                                    ,[DeliveryPostcode]
                                    ,[ConsultantName]
                                    ,[ConsultantPostcode]
                                    ,[ConsultantPhone]
                                    ,[GarmentTotal])
                           VALUES
                                    (@InvoiceDescription
                                    ,@InvoiceNumber
                                    ,@InvoiceDate
                                    ,@RunNumber
                                    ,@DeliveryName
                                    ,@DeliveryAddress
                                    ,@DeliverySuburb
                                    ,@DeliveryState
                                    ,@DeliveryPostcode
                                    ,@ConsultantName
                                    ,@ConsultantPostcode
                                    ,@ConsultantPhone
                                    ,@GarmentTotal)";

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                cmd.Parameters.AddWithNullableValue("@InvoiceDescription", print.InvoiceDescription);
                cmd.Parameters.AddWithNullableValue("@InvoiceNumber", print.InvoiceNumber);
                cmd.Parameters.AddWithNullableValue("@InvoiceDate", print.InvoiceDate);
                cmd.Parameters.AddWithNullableValue("@RunNumber", print.RunNumber);
                cmd.Parameters.AddWithNullableValue("@DeliveryName", print.DeliveryName);
                cmd.Parameters.AddWithNullableValue("@DeliveryAddress", print.DeliveryAddress);
                cmd.Parameters.AddWithNullableValue("@DeliverySuburb", print.DeliverySuburb);
                cmd.Parameters.AddWithNullableValue("@DeliveryState", print.DeliveryState);
                cmd.Parameters.AddWithNullableValue("@DeliveryPostcode", print.DeliveryPostcode);
                cmd.Parameters.AddWithNullableValue("@ConsultantName", print.ConsultantName);
                cmd.Parameters.AddWithNullableValue("@ConsultantPostcode", print.ConsultantPostcode);
                cmd.Parameters.AddWithNullableValue("@ConsultantPhone", print.ConsultantPhone);
                cmd.Parameters.AddWithValue("@GarmentTotal", print.GarmentTotal);

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        private static void ImportPrintLinesIntoDatabase(List<InvoicePDFPrintLine> printLines)
        {
            string query = @"INSERT INTO [dbo].[InvoicePDFPrintLine]
                                   ([InvoiceNumber]
                                   ,[GuestName]
                                   ,[GarmentDescription]
                                   ,[GarmentSize]
                                   ,[GarmentColour]
                                   ,[GarmentQtySupplied]
                                   ,[GarmentQtyBO]
                                   ,[GarmentPriceIncGST]
                                   ,[GarmentDepositPaid]
                                   ,[GarmentComment])
                            VALUES
                                   (@InvoiceNumber
                                   ,@GuestName
                                   ,@GarmentDescription
                                   ,@GarmentSize
                                   ,@GarmentColour
                                   ,@GarmentQtySupplied
                                   ,@GarmentQtyBO
                                   ,@GarmentPriceIncGST
                                   ,@GarmentDepositPaid
                                   ,@GarmentComment)";

            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                foreach (var line in printLines)
                {
                    using (SqlCommand cmd = new SqlCommand(query, cn))
                    {
                        cmd.Parameters.AddWithValue("@InvoiceNumber", line.InvoiceNumber);
                        cmd.Parameters.AddWithNullableValue("@GuestName", line.GuestName);
                        cmd.Parameters.AddWithNullableValue("@GarmentDescription", line.GarmentDescription);
                        cmd.Parameters.AddWithNullableValue("@GarmentSize", line.GarmentSize);
                        cmd.Parameters.AddWithNullableValue("@GarmentColour", line.GarmentColour);
                        cmd.Parameters.AddWithNullableValue("@GarmentQtySupplied", line.GarmentQtySupplied);
                        cmd.Parameters.AddWithNullableValue("@GarmentQtyBO", line.GarmentQtyBO);
                        cmd.Parameters.AddWithValue("@GarmentPriceIncGST", line.GarmentPriceIncGST);
                        cmd.Parameters.AddWithValue("@GarmentDepositPaid", line.GarmentDepositPaid);
                        cmd.Parameters.AddWithNullableValue("@GarmentComment", line.GarmentComment);

                        cmd.ExecuteNonQuery();
                    }
                }
                cn.Close();
            }
        }

    }

    public static class SqlParameterExtensions
    {
        public static SqlParameter AddWithNullableValue(this SqlParameterCollection collection, string parameterName, object value)
        {
            if (value == null)
                return collection.AddWithValue(parameterName, DBNull.Value);
            else
                return collection.AddWithValue(parameterName, value);
        }
    }
}