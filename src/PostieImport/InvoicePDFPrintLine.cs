﻿namespace PostieImport
{
    public class InvoicePDFPrintLine
    {
        public string InvoiceNumber { get; set; }
        public string GuestName { get; set; }
        public string GarmentDescription { get; set; }
        public string GarmentSize { get; set; }
        public string GarmentColour { get; set; }
        public string GarmentQtySupplied { get; set; }
        public string GarmentQtyBO { get; set; }
        public decimal GarmentPriceIncGST { get; set; }
        public decimal GarmentDepositPaid { get; set; }
        public string GarmentComment { get; set; }
    }
}
