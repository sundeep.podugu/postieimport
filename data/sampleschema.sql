
CREATE TABLE [dbo].[InvoicePDFPrint](
	[InvoicePDFPrint_ID] [int] IDENTITY(1000,1) NOT NULL,
	[InvoiceDescription] [varchar](255) NULL,
	[InvoiceNumber] [varchar](255) NULL,
	[InvoiceDate] [varchar](255) NULL,
	[RunNumber] [varchar](255) NULL,
	[DeliveryName] [varchar](255) NULL,
	[DeliveryAddress] [varchar](255) NULL,
	[DeliverySuburb] [varchar](255) NULL,
	[DeliveryState] [varchar](255) NULL,
	[DeliveryPostcode] [varchar](255) NULL,
	[ConsultantName] [varchar](255) NULL,
	[ConsultantPostcode] [varchar](255) NULL,
	[ConsultantPhone] [varchar](255) NULL,
	[GarmentTotal] [decimal](18, 2) NULL,
 CONSTRAINT [PK_InvoicePDFPrint] PRIMARY KEY CLUSTERED 
(
	[InvoicePDFPrint_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[InvoicePDFPrintLine](
	[InvoicePDFPrintLine_ID] [int] IDENTITY(1000000,1) NOT NULL,
	[InvoiceNumber] [varchar](255) NULL,
	[GuestName] [varchar](255) NULL,
	[GarmentDescription] [varchar](255) NULL,
	[GarmentSize] [varchar](255) NULL,
	[GarmentColour] [varchar](255) NULL,
	[GarmentQtySupplied] [varchar](255) NULL,
	[GarmentQtyBO] [varchar](255) NULL,
	[GarmentPriceIncGST] [decimal](18, 2) NULL,
	[GarmentDepositPaid] [decimal](18, 2) NULL,
	[GarmentComment] [varchar](255) NULL,
 CONSTRAINT [PK_InvoicePDFPrintLine] PRIMARY KEY CLUSTERED 
(
	[InvoicePDFPrintLine_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

